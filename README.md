# k8s-prep

Prepare hosts for K8s installation.

## Requirements

A group of in your Ansible inventory with all your K8s hosts.

## Role Variables

| Name          | Default | Description                                   |
| :------------ | :-----: | :-------------------------------------------- |
| `k8s_group`   |   k8s   | Name of the Ansible group with the K8s hosts. |
| `k8s_version` |  1.22   | K8s version to fit with CRI-O                 |

## Dependencies

### Collections

- `ansible.posix`
- `community.general`

## Example Playbook

```yaml
- name: Prepare K8s hosts
  hosts: k8s
  tasks:
    - name: Include K8s prep role.
      include_role:
        name: botand.k8s-prep
```

## License

GPL3

## Author Information

Andreas Botzner
